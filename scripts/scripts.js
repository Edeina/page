$(document).ready(function () {
    console.log('Slider')
    $('.slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: true
    });

    $('.choose-us-slider').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: true,
        arrows: false
    });



    $('form').submit(function (event) {
        event.preventDefault();
        var errorAlert = $('.alert-danger')
        var successAlert = $('.alert-success')
        var name = $('.name').val() /** per te marre vlen elementit qe duam  **/
        if (name.length === 0) {
            console.log(successAlert)         /** per te pyetur vendosim 3=  **/
            errorAlert.css('display', 'block')
            errorAlert.html('Ju lutem vendosni emrin!')
            successAlert.ccs('display', 'none')

        } else if (name.length < 3) {
            errorAlert.css('display', 'block')
            errorAlert.html('Emri duhet te kete me teper se dy shkronja!')
            successAlert.ccs('display', 'none')

        }
        else {
            successAlert.css('display', 'block')
            successAlert.html('Ju lutem vendosni emrin!')
            errorAlert.css('display', 'none')

        }

    })
    // init Isotope

    var $grid = $('.grid').isotope({
        // options
    });
    // filter items on button click
    $('.filter-button-group').on('click', 'button', function () {
        var filterValue = $(this).attr('data-filter');
        console.log(filterValue)
        $grid.isotope({ filter: filterValue });
    });



    //Page Smooth Scrolling
    function smoothScrolling() {
        $('a.page-scroll').click(function () {
            var target = $(this.hash);
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - 82
                }, 600);
                return false;
            }
        }
        );
    }
    smoothScrolling();

    //Menu shrink on page scroll

    function getCurrentScroll() {
        return window.pageYOffset ||
            document.documentElement.scrollTop;
    }



    $(window).scroll(function () {
        var scroll = getCurrentScroll();

        if (scroll >= 200) {
            $('header').addClass('shrink');
        } else {
            $('header').removeClass('shrink');
        }

    });


    function toggleAnimation() {

        $('.navbar-toggler').click(function () {

            $('.navbar-toggler .line').toggleClass('active');

        });

    } toggleAnimation();


});
